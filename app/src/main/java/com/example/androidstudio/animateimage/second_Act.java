package com.example.androidstudio.animateimage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class second_Act extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_);


        Button btn=(Button) findViewById(R.id.btn1);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view){
                Thread timerThread = new Thread() {
                    public void run() {
                        try {
                            sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            Intent intnt = new Intent(second_Act.this, third_screen.class);
                            startActivity(intnt);
                        }
                    }
                };
                timerThread.start();
            }
        });

        //this.btn1 =(Button) findViewById(R.id.btn1);
    }
    //public void goNext (View v)
   // {
       // Intent in = new Intent(this,third_screen.class);
       // startActivity(in);

    //}
}
