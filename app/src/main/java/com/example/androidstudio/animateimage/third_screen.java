package com.example.androidstudio.animateimage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class third_screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_screen);

        ImageView imj = (ImageView) findViewById(R.id.img4);

        imj.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch(motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Toast.makeText(third_screen.this, "PRESSED DOWN", Toast.LENGTH_SHORT).show();
                        return true;
                    case MotionEvent.ACTION_UP:
                        Toast.makeText(third_screen.this, "PRESSED UP", Toast.LENGTH_SHORT).show();
                        return true;
                }
                return false;
            }
        });
    }
}
