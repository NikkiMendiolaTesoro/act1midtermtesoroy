package com.example.androidstudio.animateimage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

        @Override
        protected void onCreate (Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            ImageView img1 =(ImageView) findViewById(R.id.img1);
            ImageView img2 = (ImageView) findViewById(R.id.img2);

            img1.animate().rotationBy(3600f).alpha(0f).setDuration(3000);
            img2.animate().rotationBy(3600f).alpha(1f).setDuration(3000);

            Thread timerThread= new Thread() {
                public void run() {
                    try {
                        sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        Intent intnt = new Intent(MainActivity.this,second_Act.class);
                        startActivity(intnt);
                        finish();
                    }
                }
            };
            timerThread.start();
        }

    //public void animateImage(View view) {
       // ImageView img1 = (ImageView) findViewById(R.id.img1);
       // ImageView img2 = (ImageView) findViewById(R.id.img2);
        //img1.animate().translationYBy(1000f).setDuration(2000);

       // if (img1.getAlpha() == 1) {
          //  img1.animate().rotationBy(3600f).alpha(0f).setDuration(2000);
            //img2.animate().rotationBy(3600f).alpha(1f).setDuration(2000);

       // }
       // else{
           // img2.animate().rotationBy(3600f).alpha(0f).setDuration(2000);
           // img1.animate().rotationBy(3600f).alpha(1f).setDuration(2000);
      //  }

        }

